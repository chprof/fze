(function(){
	var doc = $(document),
		win = $(window);

	doc.ready(function() {
		svg4everybody({
			polyfill: true
		})
		$('.about-slider').owlCarousel({
			items: 1,
			nav: true,
			dots: false,
			navText: [
				'<svg class="svg-icon svg-icon_reverse">'+
					'<use xlink:href="images/svg-sprite/svgSprite.svg#arrow"></use>'+
				'</svg>',
				'<svg class="svg-icon">'+
					'<use xlink:href="images/svg-sprite/svgSprite.svg#arrow"></use>'+
				'</svg>'
			]
		});
		$('.reviews-slider').owlCarousel({
			items: 2,
			nav: true,
			dots: false,
			margin: 30,
			navText: [
				'<svg class="svg-icon svg-icon_reverse">'+
					'<use xlink:href="images/svg-sprite/svgSprite.svg#arrow"></use>'+
				'</svg>',
				'<svg class="svg-icon">'+
					'<use xlink:href="images/svg-sprite/svgSprite.svg#arrow"></use>'+
				'</svg>'
			],
			responsive: {
				0: {
					items: 1
				},
				768: {
					items: 2
				},
			}
		});
		$('[data-toggle="toggleClass"]').click(function() {
			var target = $(this).data('target');
			$('#'+target).toggleClass('visible');
		});
		$('[data-toggle="collapse"]').click(function() {
			var firstTxt = $(this).html();
			var txt = $(this).data('toggle-text');
			console.log(firstTxt);
			console.log(txt);
			$(this).html(txt);
			$(this).data('toggle-text', firstTxt);
			firstTxt = txt;
		});
		$('.select-init').each(function() {
			var placeholder = $(this).data('placeholder');
			$(this).select2({
				placeholder: placeholder,
				allowClear: true,
				minimumResultsForSearch: Infinity,
				dropdownParent: $(this).parent()
			})
		})
		$(window).on("load",function(){
			$("[scrollbar='true']").mCustomScrollbar({
				axis: 'y',
				scrollInertia: 300
			});
		});
	});
}())