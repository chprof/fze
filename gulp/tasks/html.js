module.exports = function() {
	$.gulp.task("html", function() {
		return $.gulp.src(["./app/*.html", "./app/*.php", "./app/*.htacces", "!./app/includes/*.html"])
			.pipe($.gp.rigger())
			.pipe($.gp.cached())
			.pipe($.gulp.dest("./build/"))
			.on("end", $.bs.reload);
	});
};